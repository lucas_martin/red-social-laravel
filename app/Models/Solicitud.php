<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    use HasFactory;
    
    //
    protected $fillable = [
    	"remitente", "receptor", "valor"
    ];

    public function verRemitente(){
    	$usuario = User:: findOrFail($this -> remitente);
    	return $usuario -> name;
    }

    public function verReceptor(){
    	$usuario = User:: findOrFail($this -> receptor);
    	return $usuario -> name;
    }
}
