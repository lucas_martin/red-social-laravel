<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Publicacion extends Model
{
    use HasFactory;

    //
    protected $fillable = [
    	"user_id", "publicacion"
    ];

    public function user(){
    	return $this -> belongsTo("App\Models\User");
    }

    public function foto(){
    	return $this -> morphOne("App\Models\Foto", "fotoable");
    }
}
