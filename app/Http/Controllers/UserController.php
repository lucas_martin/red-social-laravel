<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\User;

use App\Mensaje;

use App\Solicitud;

use App\Publicacion;

use App\Models\Foto;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $usuarios = User:: all();
        return view("otros.usuarios.otros_usuarios", compact("usuarios"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = User:: findOrFail(Auth:: id());

        $entrada = $request -> all();
        if(null !== $request -> file('fot_enviada')){
            $archivo = $request -> file('fot_enviada');
            $nombre = $archivo -> getClientOriginalName();
            $archivo -> move("images/perf", $nombre);
            $foto = Foto:: create(['ruta_foto' => $nombre, 'fotoable_id' => $id, 'fotoable_type' => 'App\Models\User']);
            $entrada['foto_id'] = $foto -> id;
        }

        $user -> update($entrada);
        return redirect("/home");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
