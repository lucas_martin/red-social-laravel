<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

use App\Models\User;

class UserSinResController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function los_otros(){
        //
        return view("otros.usuarios.otros_usuarios");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function los_otros_busqueda(){
        //
        return view("otros.usuarios.otr_us_busqueda");
    }

    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_amigos()
    {
        //
        $usuarios = [];

        $solicitudess = DB:: table("solicituds")
        							->where("receptor", Auth:: id())
        							->where("valor", 1);

        $solicitudes = DB:: table("solicituds")
        							-> where("remitente", Auth::id())
        							-> where("valor", 1)
        							-> union($solicitudess)
        							-> orderBy("created_at", "asc")
        							-> get();

        foreach($solicitudes as $solic){
        	if($solic -> remitente == Auth:: id()){
        		$usuarios[] = User:: findOrFail($solic -> receptor);
        	}else if($solic -> receptor == Auth:: id()){
        		$usuarios[] = User:: findOrFail($solic -> remitente);
        	}
        }

        return view("otros.usuarios.amigos", compact("usuarios"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modificar_cuenta()
    {
        //
        $usuario = User:: findOrFail(Auth:: id());

        return view("cuenta.modificar_cuenta", compact("usuario"));
    }
}
