<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\Publicacion;

use App\Models\Foto;

class PublicacionResController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_muro()
    {
        //
        return view("otros.publicaciones.muro");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_muro_persona($id_usuario)
    {
        //        
        return view("otros.publicaciones.index_muro_persona", compact("id_usuario"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $entrada = $request -> all();

        $id_autentificacion = Auth:: id();

        $entrada['user_id'] = $id_autentificacion;

        $publicacion = Publicacion:: create($entrada);

        if(null !== $request -> file('imagen')){
            $imagen = $request -> file('imagen');
            $nombre = $imagen -> getClientOriginalName();
            $imagen -> move("images/publicac", $nombre);
            $foto = Foto:: create(['ruta_foto' => $nombre, 'fotoable_id' => $publicacion -> id, 'fotoable_type' => 'App\Models\Publicacion']);

        }

        return redirect("/home");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
