<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\Solicitud;

class SolicitudController extends Controller
{
    //
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function enviar_solicitud(Request $request)
    {
        //
        $entrada = $request -> all();
        Solicitud:: create($entrada);
        return redirect("/home");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mis_solicitudes()
    {
        //
    	$solicitudes = Solicitud:: where("receptor", Auth:: id())
    								-> where("valor", 0)
    								-> get();

    	return view("otros.solicitudes.mis_solicitudes", compact("solicitudes"));
    }
}

