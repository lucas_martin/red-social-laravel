<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\Publicacion;

use App\Models\Foto;

class PublicacionController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_muro()
    {
        //
        return view("otros.publicaciones.muro");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_muro_persona($id_usuario)
    {
        //
        return view("otros.publicaciones.index_muro_persona", compact("id_usuario"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $entrada = $request -> all();

        $id_autentificacion = Auth:: id();

        $entrada['user_id'] = $id_autentificacion;

        $publicacion = Publicacion:: create($entrada);

        if(null !== $request -> file('imagen')){
            $imagen = $request -> file('imagen');
            $nombre = $imagen -> getClientOriginalName();
            $imagen -> move("images/publicac", $nombre);
            $foto = Foto:: create(['ruta_foto' => $nombre, 'fotoable_id' => $publicacion -> id, 'fotoable_type' => 'App\Models\Publicacion']);

        }

        return redirect("/home");
    }
}
