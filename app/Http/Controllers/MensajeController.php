<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MensajeController extends Controller
{
    //
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_mensajes_vista()
    {
        //
        return view("otros.mensajes.mensajes_vista");
    }
}
