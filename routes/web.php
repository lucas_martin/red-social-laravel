<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Auth;

use App\Models\User;

use App\Models\Publicacion;

//

use App\Http\Controllers\PublicacionResController;

use App\Http\Controllers\PublicacionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route:: post("/otros/publicaciones/realizar_publicacion", "App\Http\Controllers\PublicacionResController@store");

//Route:: resource("/otros/publicaciones/_publicaciones", "PublicacionResController");

Route:: get("/multimedia/imagenes", function(){
	return view("multimedia.imagenes");

} ) -> name("/multimedia/imagenes");

Route:: get("/multimedia/pagina3", function(){
	return view("multimedia.pagina3");

} ) -> name("/multimedia/pagina3");

Route:: get("/otros/publicaciones/muro", "App\Http\Controllers\PublicacionController@index_muro") -> name("/otros/publicaciones/muro");

Route:: get("/otros/_usuarios/los_otros", "App\Http\Controllers\UserSinResController@los_otros") -> name("/otros/_usuarios/los_otros");


Route:: post("/otros/solicitudes/enviar_solicitud", "App\Http\Controllers\SolicitudController@enviar_solicitud");

Route:: get("/otros/_usuarios/los_otros_busqueda", "App\Http\Controllers\UserSinResController@los_otros_busqueda") -> name("/otros/_usuarios/los_otros_busqueda");

Route:: get("/otros/solicitudes/mis_solicitudes", "App\Http\Controllers\SolicitudController@mis_solicitudes");

Route:: resource("/otros/solicitudes/_solicitudes", "App\Http\Controllers\SolicitudResController");

Route:: get("/otros/publicaciones/{id_usuario}/index_muro_persona", "App\Http\Controllers\PublicacionController@index_muro_persona") -> name("/otros/publicaciones/{id_usuario}/index_muro_persona");

Route:: get("/otros/users/amigos", "App\Http\Controllers\UserSinResController@index_amigos") -> name("/otros/users/amigos");

Route:: resource("/otros/usuarios", "App\Http\Controllers\UserController");

Route:: get("/cuenta/modificar_cuenta", "App\Http\Controllers\UserSinResController@modificar_cuenta") -> name("/cuenta/modificar_cuenta");

Route:: get("/otros/mensajes/mensajes_vista", "App\Http\Controllers\MensajeController@index_mensajes_vista") -> name("/otros/mensajes/mensajes_vista");

