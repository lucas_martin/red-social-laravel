
<style>
    body{
        background-color:rgb(255, 164, 250);
    }

    .div1{
        padding:10px 10px 10px 10px;
        width:380px;
        height:537px;
        display:block;

        /*position:relative;*/

        /*position:absolute;*/
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        border-radius:2px 2px 2px 2px;
        margin: 0px 10px 0px 10px;

        float:left;

        position:fixed;
        /*display:flex;*/
    }

</style>

<div class='div1'>

    @yield('div1')

        <a class="dropdown-item" href="{{ route('logout') }}"
       onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
            {{ __('cerrar sesion') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        <br><br>

        @php
            use Illuminate\Support\Facades\Auth;

            use App\Models\User;

            $usuario = User:: findOrFail(Auth:: id());
            if($usuario -> foto){
                echo "<img style = 'width:50px' src = '/images/perf/" . $usuario -> foto -> ruta_foto . "'/>";
            }else{
                echo "<img style = 'width:50px' src = '/images/perf/logrostro.jpeg'/>";
            }
            echo $usuario -> name;
        @endphp

        <p>esto es informacion solo para usuarios registrados</p>

        <table>
            <tr>
                <td><a href = "/home">pagina principal</a></td>
            </tr>
        </table>
        <table>
            <tr>
                <td><a href = "/multimedia/imagenes">pagina 2</a></td>
                <td><a href = "/multimedia/pagina3">pagina 3</a></td>
            </tr>
        </table>
        <table>
            <tr>
            </tr>
            <tr>
                <td><a href = "/otros/publicaciones/muro">ir a mi muro</a></td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td><a href = "/otros/_usuarios/los_otros">encontrar gente</a></td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td><a href = "/otros/_usuarios/los_otros_busqueda">encontrar gente por busqueda</a></td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td><a href = "/otros/solicitudes/mis_solicitudes">mis solicitudes</a></td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td><a href = "/otros/users/amigos">amigos</a></td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td><a href="/otros/mensajes/mensajes_vista">mensajes</a></td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td><a href = "/cuenta/modificar_cuenta">modificar datos de usuario</a></td>
            </tr>
        </table>

</div>



