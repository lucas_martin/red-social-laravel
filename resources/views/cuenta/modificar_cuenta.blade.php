@include("enlaces.div1")
@include("otros.mensajes.mensajes")

<style>
    .div2{
        padding:10px 10px 10px 10px;
        width:380px;
        height:auto;
        display:inline-block;

        position:relative;

        /*position:relative;*/
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        border-radius:2px 2px 2px 2px;
        margin: 0px 10px 0px 430px;

        float:left;

        /*position:fixed;*/

        /*display:flex;*/
    }
</style>

<div>
    @section('div1')
    @endsection
</div>

<div class = "div2">
    <h1 style = "text-align:center">Modificar cuenta</h1>

        {!! Form:: model($usuario, ['method' => 'PATCH', 'action' => ['App\Http\Controllers\UserController@update', $usuario->id], 'files' => true ]) !!}
        <table class = "table">
            <tr>
                <td>{!! Form:: label('name', 'nombre de usuario') !!}</td> <!-- el label se tiene que llamar tambien como el campo de texto para que se muestren los datos en la pagina de editar -->
            </tr>
            <tr>
                <td>{!! Form:: text('name') !!}</td>
            </tr>
            <tr>
                <td>{!! Form:: label('email', 'mail') !!}</td>
            </tr>
            <tr>
                <td>{!! Form:: text('email') !!}</td>
            </tr>
            <tr>
                <td><img width = "50" src = "/images/perf/{{ $usuario -> foto ? $usuario -> foto -> ruta_foto : 'logrostro.jpeg' }}"></td>
            </tr>
            <tr>
                <td>{!! Form:: file('fot_enviada') !!}</td>
            </tr>
            <tr>
                <td>{!! Form:: submit('modificar usuario') !!}</td>
            </tr>
            <tr>
                <td>{!! Form:: reset('borrar campos') !!}</td>
            </tr>
        </table>
    {!! Form:: close() !!}
    <br>
    <a href = "/cuenta/confirmar_eliminar">eliminar cuenta</a>
</div>

<div>
    @section("div3")
    @endsection
</div>
