@include("enlaces.div1")
@include("otros.mensajes.mensajes")

<style>
    .div2{
        padding:10px 10px 10px 10px;
        width:380px;
        height:auto;
        display:inline-block;

        position:relative;

        /*position:relative;*/
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        border-radius:2px 2px 2px 2px;
        margin: 0px 10px 0px 430px;

        float:left;

        /*position:fixed;*/

        /*display:flex;*/
    }

</style>

<div>
    @section('div1')
    @endsection
</div>

<div class = "div2">

    <h2>pagina de inicio</h2>

    {{-- opcion de publicar --}}

    @if (session('status'))
        {{ session('status') }}
    @endif

    You are logged in!


    <h2>realizar publicacion</h2>
    {!! Form:: open(['method' => 'POST', 'action' => 'App\Http\Controllers\PublicacionResController@store', 'enctype' => 'multipart/form-data', 'files' => true]) !!}
        <table>
            <tr>
                <textarea name='publicacion' rows=3 cols=45></textarea>
            </tr>
            <tr>
                <input type = "file" name = "imagen">
            </tr>
            <tr>
                <td>{!! Form:: submit('PUBLICAR')!!}</td>
                <td>{!! Form:: reset('BORRAR CAMPOS') !!}</td>
            </tr>
        </table>
    {!! Form:: close() !!}

    {{-- ver publicaciones de todos --}}

    @include("otros.publicaciones.index_publicaciones")

</div>

<div>
    @section("div3")
    @endsection
</div>
