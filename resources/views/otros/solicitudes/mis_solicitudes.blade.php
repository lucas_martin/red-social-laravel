@include("enlaces.div1")
@include("otros.mensajes.mensajes")

<style>
    .div2{
        padding:10px 10px 10px 10px;
        width:380px;
        height:auto;
        display:inline-block;

        position:relative;

        /*position:relative;*/
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        border-radius:2px 2px 2px 2px;
        margin: 0px 10px 0px 430px;

        float:left;

        /*position:fixed;*/

        /*display:flex;*/
    }
</style>

<div>
    @section('div1')
    @endsection
</div>

<div class = "div2">

    @php
        use Illuminate\Support\Facades\Auth;

        use App\Models\Solicitud;

        use App\Models\User;

        $id_autentificado = Auth:: id();

        //$solicitudes = Solicitud:: where("receptor", $id_autentificado) -> get();

    @endphp



        @if(sizeof($solicitudes) > 0)
            @foreach($solicitudes as $solic)


                @php
                    $id_solicitud = $solic -> id;

                    $id_us_remitente = $solic -> remitente;

                    $us_remitente = User:: findOrFail($id_us_remitente);

                    if($us_remitente -> foto){
                        $fot_remitente = $us_remitente -> foto -> ruta_foto;
                    }else{
                        $fot_remitente = "logrostro.jpeg";
                    }
                @endphp

                    <img width = "90" src = "/images/perf/{{ $fot_remitente }}"><br>
                    {{ $solic -> verRemitente() }}




                {!! Form:: model($solic, ['method' => 'PATCH', 'action' => ['App\Http\Controllers\SolicitudResController@update', $id_solicitud] ]) !!}
                    <input type = "hidden" name = "remitente" value = "{{ $id_autentificado }}">
                    <input type = "hidden" name = "receptor" value = "{{ $solic -> remitente }}">
                    {!! Form:: submit('AGREGAR A AMIGOS') !!}
                {!! Form:: close() !!} <br><br>


            @endforeach
        @else
            <p>No hay solicitudes</p>
        @endif
    </table>
</div>

<div>
    @section("div3")
    @endsection
</div>
