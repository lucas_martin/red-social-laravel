@include("enlaces.div1")
@include("otros.mensajes.mensajes")

<style>
    .div2{
        padding:10px 10px 10px 10px;
        width:380px;
        height:auto;
        display:inline-block;

        position:relative;

        /*position:relative;*/
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        border-radius:2px 2px 2px 2px;
        margin: 0px 10px 0px 430px;

        float:left;

        /*position:fixed;*/

        /*display:flex;*/
    }
</style>

<div>
    @section('div1')
    @endsection
</div>

<div class = "div2">

    @php

        use App\Models\Publicacion;

        use Illuminate\Support\Facades\DB;


        $tamagno_paginas = 15;

        if(isset($_GET["pagina"])){
            if($_GET["pagina"]==1){
                //return redirect("/otros/_usuarios/los_otros");
                $pagina = 1;
            }else{
                $pagina=$_GET["pagina"];
            }
        }else{
            $pagina=1;
        }


        $publ_finales = [];


        /*$sql_total = User:: all();*/
        $tod_publicaciones = Publicacion:: where("user_id", $id_usuario)
                                    -> orderBy("created_at", "desc")
                                    -> get();

        $cont_publicaciones = 0;
        foreach($tod_publicaciones as $publ){
            $cont_publicaciones += 1;
        }

        /*echo $cont_publicaciones . "<br>";*/

        $total_paginas=ceil($cont_publicaciones/$tamagno_paginas);

        if($total_paginas == 0){
            $total_paginas = 1;
        }

        $publicaciones_finales = [];

        if($pagina == $total_paginas){

            if($cont_publicaciones != 0){
                for($i = 0; $i < $cont_publicaciones; $i++){
                    $publicaciones_finales[] = $tod_publicaciones[$i];
                }
            }

        }else{

            for ($i = 0; $i < ($tamagno_paginas * $pagina); $i++){
                $publicaciones_finales[] = $tod_publicaciones[$i];
            }
        }

    @endphp

    @if(null !== $publicaciones_finales)
        @foreach($publicaciones_finales as $publ)

            <b>{{ $publ -> user -> name }}</b> <br>

            {{ $publ -> publicacion}} <br>

            @if($publ -> foto)
                <img width = "370" src = "/images/publicac/{{ $publ -> foto -> ruta_foto }}">
            @endif

            <br><br>

        @endforeach
    @endif

    @php

        if($pagina == $total_paginas){
            if($cont_publicaciones == 0){
                echo "<b>no hay publicaciones</b><br>";
            }else{
                echo "<b>no hay mas publicaciones</b><br>";
            }

        }else{
            echo "ir a pagina siguiente:<br>";

            $pagina_actual = $_SERVER['PHP_SELF'];
            $pagina_siguiente = $pagina + 1;
            echo "
                <form action='$pagina_actual' method='get'>
                    <input type = 'hidden' name = 'pagina' value = '$pagina_siguiente'>
                    <input text-align = 'center' type = 'submit' value = '▼'>
                </form>";
        }

    @endphp

</div>

<div>
    @section("div3")
    @endsection
</div>
