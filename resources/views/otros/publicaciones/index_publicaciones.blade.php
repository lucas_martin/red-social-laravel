

    @php
        use App\Models\Publicacion;

        use Illuminate\Support\Facades\DB;

        //

        use Illuminate\Support\Facades\Auth;

        $tamagno_paginas = 15;

        if(isset($_GET["pagina"])){
            if($_GET["pagina"]==1){
                //return redirect("/otros/_usuarios/los_otros");
                $pagina = 1;
            }else{
                $pagina=$_GET["pagina"];
            }
        }else{
            $pagina=1;
        }


        $publ_finales = [];


        /*$sql_total = User:: all();*/
        $tod_publicaciones = Publicacion:: orderBy("created_at", "desc")
                                    -> get();
        /*$busq_usuarios = DB:: select("SELECT * FROM users limit $empezar_desde,$tamagno_paginas");*/
        /*$busq_usuarios = User:: limit($empezar_desde) -> get();*/


        $solicitudess = DB:: table("solicituds")
                                    ->where("receptor", Auth:: id())
                                    ->where("valor", 1);

        $solicitudes = DB:: table("solicituds")
                                    -> where("remitente", Auth::id())
                                    -> where("valor", 1)
                                    -> union($solicitudess)
                                    -> get();

        $bandera = False;
        foreach($tod_publicaciones as $publ){
            foreach($solicitudes as $solic){
                if($solic -> remitente == Auth:: id()){
                    if($publ -> user_id == $solic -> receptor){
                        $publ_finales[] = $publ;
                        break;
                    }
                }
                if($solic -> receptor == Auth:: id()){
                    if($publ -> user_id == $solic -> remitente){
                        $publ_finales[] = $publ;
                        break;
                    }
                }
            }
            if($publ -> user_id == Auth:: id()){
                $publ_finales[] = $publ;
            }
        }

        /*$empezar_desde=($pagina-1)*$tamagno_paginas;*/


        $cont_publicaciones = 0;
        foreach($publ_finales as $publ){
            $cont_publicaciones += 1;
        }

        /*echo $cont_publicaciones . "<br>";*/

        $total_paginas=ceil($cont_publicaciones/$tamagno_paginas);

        if($total_paginas == 0){
            $total_paginas = 1;
        }

        $publicaciones_finales = [];

        if($pagina == $total_paginas){
            /*$i = $empezar_desde;*/

            $cont_publicaciones = 0;
            foreach($publ_finales as $publ){
                $cont_publicaciones += 1;
            }

            if($cont_publicaciones != 0){
                for($i = 0; $i < $cont_publicaciones; $i++){
                    $publicaciones_finales[] = $publ_finales[$i];
                }
            }

        }else{
            /*$i = $empezar_desde;*/

            $cont_publicaciones = 0;
            foreach($publ_finales as $publ){
                $cont_publicaciones += 1;
            }

            for ($i = 0; $i < ($tamagno_paginas * $pagina); $i++){
                $publicaciones_finales[] = $publ_finales[$i];
            }
        }

    @endphp


    @if($publicaciones_finales)
        @foreach($publicaciones_finales as $publ)

            <b>{{ $publ -> user -> name }}</b> <br>

            {{ $publ -> publicacion}} <br>

            @if($publ -> foto)
                <img width = "370" src = "/images/publicac/{{ $publ -> foto -> ruta_foto }}">
            @endif

            <br><br>

        @endforeach
    @endif

    @php

        if($pagina == $total_paginas){
            if($cont_publicaciones == 0){
                echo "<b>no hay publicaciones</b><br>";
            }else{
                echo "<b>no hay mas publicaciones</b><br>";
            }

        }else{
            echo "ir a pagina siguiente:<br>";

            $pagina_actual = $_SERVER['PHP_SELF'];
            $pagina_siguiente = $pagina + 1;
            echo "
                <form action='$pagina_actual' method='get'>
                    <input type = 'hidden' name = 'pagina' value = '$pagina_siguiente'>
                    <input text-align = 'center' type = 'submit' value = '▼'>
                </form>";
        }

    @endphp
