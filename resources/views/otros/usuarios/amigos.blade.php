@include("enlaces.div1")
@include("otros.mensajes.mensajes")

@php

    use Illuminate\Support\Facades\Auth;

    use App\Models\User;

    use App\Models\Solicitud;

    $id_autentificado = Auth:: id();

@endphp

<style>
    .div2{
        padding:10px 10px 10px 10px;
        width:380px;
        height:auto;
        display:inline-block;

        position:relative;

        /*position:relative;*/
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        border-radius:2px 2px 2px 2px;
        margin: 0px 10px 0px 430px;

        float:left;

        /*position:fixed;*/

        /*display:flex;*/
    }
</style>

<div>
    @section('div1')
    @endsection
</div>

<div class = "div2">
        @if($usuarios)
            @foreach($usuarios as $us)

                <img width = "90" src = "/images/perf/{{ $us -> foto ? $us -> foto -> ruta_foto : 'logrostro.jpeg' }}"/> {{-- operador ternario --}}
                {{ $us -> name }} <br>
                {{ $us -> email }} <br><br>

            @endforeach
        @endif
</div>

<div>
    @section("div3")
    @endsection
</div>

