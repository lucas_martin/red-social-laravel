@include("enlaces.div1")
@include("otros.mensajes.mensajes")

@php

    use Illuminate\Support\Facades\Auth;

    use App\Models\User;

    $id_autentificado = Auth:: id();

@endphp

<style>
    .div2{
        padding:10px 10px 10px 10px;
        width:380px;
        height:auto;
        display:inline-block;

        position:relative;

        /*position:relative;*/
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        border-radius:2px 2px 2px 2px;
        margin: 0px 10px 0px 430px;

        float:left;

        /*position:fixed;*/

        /*display:flex;*/
    }
</style>

<script>
function enviar_formulario($i){
    document.getElementById("paginaaa").value = $i;
    document.formulario1.submit()
}
</script>

<div>
    @section('div1')
    @endsection
</div>

<div class = "div2">
    @php
        if(isset($_GET['us_busq'])){
            $usuarioBusq=$_GET['us_busq'];
        }/*else{
            header("location:/home");
        }*/
        if(isset($_GET['us_busq2'])){
            $usuarioBusq=$_GET['us_busq2'];
        }

    @endphp

        <br>nombre de usuario que desea buscar:<br>

        <form method="get" action="{{ $_SERVER['PHP_SELF'] }}">
            <table>
                <tr>
                    <td><input type="text" name="us_busq" id="us_busq"></td>
                    <td colspan="2"><input type="submit" name="buscando" value="BUSCAR"></td>
                </tr>
            </table>
        </form>
        {{-- <br><a href="pagina.php">volver a la pagina principal</a> --}}

    @php


        use Illuminate\Support\Facades\DB;

            $tamagno_paginas=5;

            if(isset($_GET["pagina"])){
                if($_GET["pagina"]==1){
                    //return redirect("/otros/_usuarios/los_otros");
                    $pagina = 1;
                }else{
                    $pagina=$_GET["pagina"];
                }
            }else{
                $pagina=1;
            }

            $busq_usuarios = [];

            $empezar_desde=($pagina)*$tamagno_paginas;

            if(isset($_GET['us_busq']) or isset($_GET['us_busq2'])){
                if(isset($_GET['us_busq'])){
                    $usuarioBusq=$_GET['us_busq'];
                }else{
                    $usuarioBusq=$_GET['us_busq2'];
                }

                $sql_total = User:: where("name", "like", "%$usuarioBusq%") -> get();

                $busq_usuarios = User:: where("name", "like", "%$usuarioBusq%") -> limit($empezar_desde) -> get();
            }else{
                /*$sql_total= DB:: select("SELECT name FROM users");*/
                $sql_total = User:: all();
                /*$busq_usuarios = DB:: select("SELECT * FROM users limit $empezar_desde,$tamagno_paginas");*/
                $busq_usuarios = User:: limit($empezar_desde) -> get();

                $usuarioBusq = "";
            }

            /*$num_filas = sizeof($sql_total);*/
            $num_filas = 0;
            foreach($sql_total as $us){
                $num_filas += 1;
            }

            $total_paginas=ceil($num_filas/$tamagno_paginas);

            if($total_paginas<=0){
                $total_paginas = 1;
            }

            echo "mostrando la pagina " . $pagina . " de " . $total_paginas . "<br><br>";

    @endphp

    @if($busq_usuarios)
            @foreach($busq_usuarios as $us)

                        @php $us_actual = User:: findOrFail($us->id); @endphp

                        <img width = "30" src = "/images/perf/{{ $us_actual -> foto ? $us_actual -> foto -> ruta_foto : 'logrostro.jpeg' }}"/> <!-- operador ternario -->


                        @php

                        $tod_usuarios = User:: all();

                        $solicitudess = DB:: table("solicituds")
                                                    ->where("receptor", Auth:: id());

                        $solicitudes = DB:: table("solicituds")
                                                    -> where("remitente", Auth::id())
                                                    -> union($solicitudess)
                                                    -> get();

                        $sol_aprobada = False;
                        $sol_no_aprobada = False;
                        foreach($solicitudes as $solic){
                            if($solic -> remitente == Auth::id()){
                                if($solic -> receptor == $us -> id){
                                    if($solic -> valor == 1){
                                        $sol_aprobada = True;
                                    }else if($solic -> valor == 0){
                                        $sol_no_aprobada = True;
                                    }
                                }
                            }
                            if($solic -> receptor == Auth::id()){
                                if($solic -> remitente == $us -> id){
                                    if($solic -> valor == 1){
                                        $sol_aprobada = True;
                                    }else if($solic -> valor == 0){
                                        $sol_no_aprobada = True;
                                    }
                                }
                            }
                        }

                        if($sol_aprobada){

                            echo "
                                <a href = '/otros/publicaciones/$us->id/index_muro_persona'>$us->name</a>
                            ";

                        }else if($us -> id == Auth:: id()){

                            echo "
                                <a href = '/otros/publicaciones/muro'>$us->name</a>
                            ";

                        }else if($sol_no_aprobada){
                            echo $us -> name;
                        }

                        /*us -> email*/

                            if(!($sol_aprobada or $sol_no_aprobada or $us -> id == Auth:: id())){

                                echo $us -> name;

                        @endphp

                            {!! Form:: open(['method' => 'POST', 'action' => 'SolicitudController@enviar_solicitud']) !!} {{-- para el formulario de 'agregar amigos' se debera recorrer el arreglo, y las solicitudes al mismo tiempo, y si alguna solicitud coincide con el id autentificado, no mostrar dicho amigo con la funcion 'unset' --}}
                                <input type = "hidden" name = "remitente" value = "{{ $id_autentificado }}">
                                <input type = "hidden" name = "receptor" value = "{{ $us -> id }}">
                                {!! Form:: submit('AGREGAR') !!}
                            {!! Form:: close() !!}

                        @php
                            }else{
                                echo "<br>";
                            }

                        @endphp

            @endforeach
        @endif



        @php

            if($pagina == $total_paginas){
                echo "<b>no hay mas usuarios con la busqueda de '$usuarioBusq'</b><br>";
            }else{
                echo "ir a pagina siguiente:<br>";

                $pagina_actual = $_SERVER['PHP_SELF'];
                $pagina_siguiente = $pagina + 1;
                echo "
                    <form action='$pagina_actual' method='get'>
                        <input type = 'hidden' name = 'pagina' value = '$pagina_siguiente'>
                        <input type = 'hidden' name = 'us_busq2' value = '$usuarioBusq'>
                        <input text-align = 'center' type = 'submit' value = '▼'>
                    </form>";
            }

        @endphp

</div>

<div>
    @section("div3")
    @endsection
</div>
