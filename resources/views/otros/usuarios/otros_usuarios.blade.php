@include("enlaces.div1")

@php

    use Illuminate\Support\Facades\Auth;

    $id_autentificado = Auth:: id();

@endphp

<style>
    .div2{
        padding:10px 10px 10px 10px;
        width:380px;
        height:auto;
        display:inline-block;

        position:relative;

        /*position:relative;*/
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        border-radius:2px 2px 2px 2px;
        margin: 0px 10px 0px 430px;

        float:left;

        /*position:fixed;*/

        /*display:flex;*/
    }
</style>

<div>
    @section('div1')
    @endsection
</div>

<div class = "div2">

    {{-- <table border = "1">
        <tr>
            <td>id</td><td>foto</td><td>nombre</td><td>mail</td><td>agregar a amigos</td>
        </tr> --}}


    @php

        /*try{*/

            use App\Models\User;

            $tamagno_paginas=7;

            if(isset($_GET["pagina"])){
                if($_GET["pagina"]==1){
                    //return redirect("/otros/_usuarios/los_otros");
                    $pagina = 1;
                }else{
                    $pagina=$_GET["pagina"];
                }
            }else{
                $pagina=1;
            }

            /*$sql_total="select nombre from usuarios";
            $resultado=$base->prepare($sql_total);
            $resultado->execute(array());*/



            /*$usuarios = [];

            $ids_filtrados = [];*/

            $tod_usuarios = User:: all();

            /*$solicitudess = DB:: table("solicituds")
                                        ->where("receptor", Auth:: id());

            $solicitudes = DB:: table("solicituds")
                                        -> where("remitente", Auth::id())
                                        -> union($solicitudess)
                                        -> get();

            foreach($solicitudes as $solic){
                if($solic -> remitente == Auth::id()){
                    $ids_filtrados[] = $solic -> receptor;
                }
                if($solic -> receptor == Auth::id()){
                    $ids_filtrados[] = $solic -> remitente;
                }
            }
            $ids_filtrados[] = Auth:: id();

            $bandera = True;
            foreach($tod_usuarios as $us){
                foreach($ids_filtrados as $idf){
                    if($us -> id == $idf){
                        $bandera = False;
                        break;
                    }
                }
                if($bandera == True){
                    $usuarios[] = $us;
                }
                $bandera = True;
            }*/

            $cont_usuarios = 0;
            foreach($tod_usuarios as $us){
                $cont_usuarios += 1;
            }

            /*$num_filas=$resultado->rowCount();*/

            $empezar_desde=($pagina-1)*$tamagno_paginas;

            $total_paginas=ceil($cont_usuarios/$tamagno_paginas);

            /*echo "numero de registros de la consulta: " . $num_filas . "<br>";
            echo "mostramos " . $tamagno_paginas . " registros por pagina <br>";*/
            echo "mostrando la pagina " . $pagina . " de " . $total_paginas . "<br><br>";

            /*$sql_limite="select nombre from usuarios limit $empezar_desde,$tamagno_paginas";*/

            $us_finales = [];

            if($pagina == $total_paginas){
                $i = $empezar_desde;

                /*$cont_usuarios = 0;
                foreach($usuarios as $us){
                    $cont_usuarios += 1;
                }*/

                for($i = $empezar_desde; $i < $cont_usuarios; $i++){
                    $us_finales[] = $tod_usuarios[$i];
                }
            }else{
                for ($i = $empezar_desde; $i < ($empezar_desde + $tamagno_paginas); $i++){
                    $us_finales[] = $tod_usuarios[$i];
                }
            }



            /*$resultado=$base->prepare($sql_limite);

            $resultado->execute(array());

            while($registro=$resultado->fetch(PDO::FETCH_ASSOC)){

                $nombUs = $registro["nombre"];

                echo "nombre de usuario: " . $nombUs . "." .
                "<br>
                <form method = 'post' action = 'agregar-a-amigos.php'>
                    <input type='hidden' name='usuario' id='usuario' value='$nombUs'>
                    <input type='submit' name='agregando' value='AGREGAR' style='margin:0; padding:0'>
                </form>
                " .
                "<br>";
            }

            $resultado->closeCursor();
        }catch(Exception $e){
            die("error: " . $e->getMessage());
        }*/
    @endphp



        @if($us_finales)
            @foreach($us_finales as $us)

                @php $us_actual = User:: findOrFail($us->id); @endphp

                <img width = "30" src = "/images/perf/{{ $us_actual -> foto ? $us_actual -> foto -> ruta_foto : 'logrostro.jpeg' }}"/> <!-- operador ternario -->


                @php

                $tod_usuarios = User:: all();

                $solicitudess = DB:: table("solicituds")
                                            ->where("receptor", Auth:: id());

                $solicitudes = DB:: table("solicituds")
                                            -> where("remitente", Auth::id())
                                            -> union($solicitudess)
                                            -> get();

                $sol_aprobada = False;
                $sol_no_aprobada = False;
                foreach($solicitudes as $solic){
                    if($solic -> remitente == Auth::id()){
                        if($solic -> receptor == $us -> id){
                            if($solic -> valor == 1){
                                $sol_aprobada = True;
                            }else if($solic -> valor == 0){
                                $sol_no_aprobada = True;
                            }
                        }
                    }
                    if($solic -> receptor == Auth::id()){
                        if($solic -> remitente == $us -> id){
                            if($solic -> valor == 1){
                                $sol_aprobada = True;
                            }else if($solic -> valor == 0){
                                $sol_no_aprobada = True;
                            }
                        }
                    }
                }

                if($sol_aprobada){

                    echo "
                        <a href = '/otros/publicaciones/$us->id/index_muro_persona'>$us->name</a>
                    ";

                }else if($us -> id == Auth:: id()){

                    echo "
                        <a href = '/otros/publicaciones/muro'>$us->name</a>
                    ";

                }else if($sol_no_aprobada){
                    echo $us -> name;
                }

                /*us -> email*/

                    if(!($sol_aprobada or $sol_no_aprobada or $us -> id == Auth:: id())){

                        echo $us -> name;

                @endphp

                    {!! Form:: open(['method' => 'POST', 'action' => 'App\Http\Controllers\SolicitudController@enviar_solicitud']) !!} {{-- para el formulario de 'agregar amigos' se debera recorrer el arreglo, y las solicitudes al mismo tiempo, y si alguna solicitud coincide con el id autentificado, no mostrar dicho amigo con la funcion 'unset' --}}
                        <input type = "hidden" name = "remitente" value = "{{ $id_autentificado }}">
                        <input type = "hidden" name = "receptor" value = "{{ $us -> id }}">
                        {!! Form:: submit('AGREGAR') !!}
                    {!! Form:: close() !!}

                @php
                    }else{
                        echo "<br>";
                    }

                @endphp

            @endforeach
        @endif
    {{-- </table> --}}
    @php
        echo "enlaces a paginas:<br>";
        for($i=1; $i<=$total_paginas; $i++){
            echo "<a href='?pagina=" . $i . " ' title='Pagina " . $i . "'>.</a>  ";
        }
    @endphp
</div>

<div>
    @section("div3")
    @endsection
</div>
