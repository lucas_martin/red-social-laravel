@include("enlaces.div1")

<style>

    .div2{
        /*padding:20px 20px 20px 20px;*/
        width:380px;
        height:557px;
        display:inline-block;

        /*position:relative;*/

        /*position:absolute;*/
        /*box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);*/
        border-radius:2px 2px 2px 2px;
        margin: 0px 10px 0px 430px;

        float:left;

        position:fixed;
        /*display:flex;*/
    }

    .div3{
        /*padding:20px 20px 20px 20px;*/
        width:380px;
        height:557px;
        display:inline-block;

        /*position:relative;*/

        /*position:absolute;*/
        /*box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);*/
        border-radius:2px 2px 2px 2px;
        margin: 0px 10px 0px 850px;

        float:left;

        position:fixed;
        /*display:flex;*/
    }

    .divA{

        padding:2px 2px 2px 2px;
        width:376px;
        display:block;
        /*position:absolute;*/
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        border-radius:2px 2px 2px 2px;
        margin: 0px 0px 0px 0px;

        height:557px;
        overflow-y: scroll;
    }

    .divB{

        padding:2px 2px 2px 2px;
        width:376px;
        display:block;
        /*position:absolute;*/
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        border-radius:2px 2px 2px 2px;
        /*margin: 5px 10px 0px 10px;*/

        height:505px;
        overflow-y: scroll;
    }

    .divC{

        /*padding:20px 20px 20px 20px;*/
        padding:10px 0px 5px 5px;
        width:376px;
        display:block;
        /*position:absolute;*/
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        border-radius:2px 2px 2px 2px;
        /*margin: 1px 10px 0px 10px;*/

        height:35px;
        /*overflow-y: scroll;*/
    }
</style>


{{-- envio de formulario --}}
{{-- <script>
function enviar_formulario(){
   document.formulario1.submit()
}
</script> --}}

<div>
    @section("div1")
    @endsection
</div>


<div class = "div3">

    @yield("div3")

    <div class='divA'>
        @php

        use Illuminate\Support\Facades\Auth;

        use Illuminate\Support\Facades\DB;

        use App\Models\User;

        use App\Models\Mensaje;

        use App\Models\Solicitud;

        $id_autentificado = Auth:: id();

        $usuarioActual = User:: findOrFail($id_autentificado);

        if(isset($_GET['amigoChat']) and isset($_GET['mensajeUsuario'])){

            $mensajeUsuario=$_GET['mensajeUsuario'];

            $amigoChat = $_GET['amigoChat'];

            $nuevo_mensaje = Mensaje:: create([
                "remitente" => $id_autentificado,
                "receptor" => $amigoChat,
                "mensaje" => $mensajeUsuario,

            ]);

            $_GET['amigoChat'] = "";

        }


        $remitente='';
        $receptor='';

        $solicitudes = Solicitud:: where("remitente", $id_autentificado)
                                    -> where("valor", 1)
                                    -> get();

        $solicitudes2 = Solicitud:: where("receptor", $id_autentificado)
                                    -> where("valor", 1)
                                    -> get();

        echo "amigos:" . "<br>";

        foreach($solicitudes as $solic){

            //echo $remit . "<br>";
            $ver_amigo = $solic -> verReceptor();
            $usuario = User:: findOrFail($solic -> receptor);

            if($usuario -> foto){
                $ruta_foto = $usuario -> foto -> ruta_foto;
            }else{
                $ruta_foto = "logrostro.jpeg";
            }


            echo "
            <form action='" . $_SERVER['PHP_SELF'] . "' method = 'get' name='formulario1'>
            <input type='hidden' value='$solic->receptor' name='amigoChat'>
            <img width = '50' src = '/images/perf/$ruta_foto'>
            <input type = 'submit' value = '$ver_amigo'>
            </form>

            " . "<br/>";
        }

        foreach($solicitudes2 as $solic2){
            //echo $solic2 -> verRemitente() . "<br>";

            //echo $remit . "<br>";
            $ver_amigo = $solic2 -> verRemitente();
            $usuario = User:: findOrFail($solic2 -> remitente);
            if($usuario -> foto){
                $ruta_foto = $usuario -> foto -> ruta_foto;
            }else{
                $ruta_foto = "logrostro.jpeg";
            }

            echo "
            <form action='" . $_SERVER['PHP_SELF'] . "' method = 'get' name='formulario1'>
            <input type='hidden' value='$solic2->remitente' name='amigoChat'>
            <img width = '50' src = '/images/perf/$ruta_foto'>
            <input type = 'submit' value = '$ver_amigo'>
            </form>

            " . "<br/>"; //formulario
        }


        @endphp

    </div>

</div>

<div class = "div2">

    <div class='divB'>


        @php

        //$amigoChat = $_GET['amigoChat'];

        if(isset($_GET['amigoChat'])){

            $amigoChat = $_GET['amigoChat'];


            echo "<br>mensajes:<br>";

            /*$result = mysqli_query($link, "SELECT * FROM mensajes where (remitente='$usuarioActual' or receptor='$usuarioActual') and (remitente='$amigoChat' or receptor='$amigoChat')");*/

            $mensajess = DB:: table("mensajes")
                                        -> where("remitente", $amigoChat)
                                        -> where("receptor", $id_autentificado);

            $mensajes = DB:: table("mensajes")
                                        -> where("remitente", $id_autentificado)
                                        -> where("receptor", $amigoChat)
                                        -> union($mensajess)
                                        -> orderBy("created_at", "asc")
                                        -> get();

            foreach($mensajes as $mens){
                if($mens -> remitente == $id_autentificado){
                    echo "
                        <p style='text-align:right'>" . $mens->mensaje . "</p><br>
                    ";
                }else if($mens -> receptor == $id_autentificado){
                    echo "
                        <p style='text-align:left'>" . $mens->mensaje . "</p>
                    ";
                }
            }

        }

        @endphp

    </div>

    <div class='divC'>


        <form method='get' action='@php echo $_SERVER['PHP_SELF']; @endphp'>
        <input type='hidden' value='@php
            if(isset($_GET['amigoChat'])){
                echo $_GET['amigoChat'];
            }
        @endphp' name='amigoChat'>
        <input type='text' name='mensajeUsuario'>
        <input type='submit' value='ENVIAR'>
        </form>

    </div>

</div>
